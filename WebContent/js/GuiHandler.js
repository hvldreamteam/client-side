"use strict";
class GuiHandler {
	constructor(){
		this.allstatuses = [];
		this.deleteTaskCallback = function(id){};
		this.newStatusCallback = function(id, status){};
	}
	
	
	showTask(task) {
		let div = document.getElementById("tasks");
		let table = div.firstChild;
		let taskExists = false;
		let id = task.id;
		
		// check if table exists
		if(table === null) {
			table = document.createElement("table");
			let thead = table.createTHead();
			let row = thead.insertRow(0);
			let cell = row.insertCell(0);
			cell.innerHTML = "Task";
			cell = row.insertCell(1);
			cell.innerHTML = "Status";
			
			let tbody = document.createElement("tbody");
			table.appendChild(tbody);
			div.appendChild(table);
		} else {
			//check if task exists
			for (let i = 0, row; row = table.rows[i]; i++) {
				if (row.id == id) {
					taskExists = true;
				}
			}
		}
			
		if(!taskExists) {
			let tbody = table.getElementsByTagName('tbody')[0];
			let row = tbody.insertRow(0);
			row.id = id;
			let cell = row.insertCell(0);
			cell.innerHTML = task.title;
			cell = row.insertCell(1);
			cell.innerHTML = task.status;
			
			//make modify element
			let select = document.createElement("select");
			let option = document.createElement("option");
			option.value = 0;
			option.text = "<Modify>";
			select.appendChild(option);
			for (let i = 0; i < this.allstatuses.length; i++) {
				let option = document.createElement("option");
				option.value = this.allstatuses[i];
				option.text = this.allstatuses[i];
				select.appendChild(option);
			}
			
			cell = row.insertCell(2);
			cell.appendChild(select);
			
			//make remove button
			cell = row.insertCell(3);
			let button = document.createElement("button");
			button.innerText = "Remove";
			button.addEventListener("click", () => this.deleteTaskCallback(id), false);
			cell.appendChild(button);
			}
		
			document.getElementById("message").innerHTML = `Found ${table.rows.length - 1} tasks`;
		
	}
	
	update(task){
		let div = document.getElementById("tasks");
		let table = div.firstChild;
		
		for (let i = 0, row; row = table.rows[i]; i++) {
			if (row.id == task.id) {
				row.cells[1].innerHTML = task.status;	
			}
		}
			
	}
	
	removeTask(id) {
		let div = document.getElementById("tasks");
		let table = div.firstChild;
		
		for (let i = 0, row; row = table.rows[i]; i++) {
			if (row.id == id) {
				table.deleteRow(i);
			}
		}		
	}
	
	noTask() {
		let div = document.getElementById("tasks");
		let table = div.firstChild;
		return table.rows.length < 2;
	}
		
}

